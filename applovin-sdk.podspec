#
# Be sure to run `pod lib lint applovin-sdk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'applovin-sdk'
  s.version          = '4.2.1'
  s.summary          = 'applovin-sdk.'

  s.description      = <<-DESC
AppLovin-SDK..
                       DESC

  s.homepage         = 'https://bitbucket.org/opendoorapp/applovin-sdk.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'opendoorapp' => 'dev@opendoorapp.com' }
  s.source           = { :git => 'https://bitbucket.org/opendoorapp/applovin-sdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '7.0'
  
  s.frameworks = 'AdSupport', 'AVFoundation', 'CoreTelephony', 'CoreGraphics', 'CoreMedia', 'StoreKit', 'SystemConfiguration', 'UIKit'
  s.vendored_frameworks = 'applovin-ios-sdk-4.2.1/AppLovinSDK.framework'

end
