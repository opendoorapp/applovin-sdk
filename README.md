# applovin-sdk

[![CI Status](http://img.shields.io/travis/mars-lan/applovin-sdk.svg?style=flat)](https://travis-ci.org/mars-lan/applovin-sdk)
[![Version](https://img.shields.io/cocoapods/v/applovin-sdk.svg?style=flat)](http://cocoapods.org/pods/applovin-sdk)
[![License](https://img.shields.io/cocoapods/l/applovin-sdk.svg?style=flat)](http://cocoapods.org/pods/applovin-sdk)
[![Platform](https://img.shields.io/cocoapods/p/applovin-sdk.svg?style=flat)](http://cocoapods.org/pods/applovin-sdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

applovin-sdk is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "applovin-sdk"
```

## Author

mars-lan, dev@opendoorapp.com

## License

applovin-sdk is available under the MIT license. See the LICENSE file for more info.
